<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
<br/>
<c:forEach items="${users}" var="user" varStatus="status"> 
  <p>${user.name} ${user.surname} ${user.email} ${fn:length(user.authorities)}
       <c:forEach items="${user.authorities}" var="authority" varStatus="status">
            ${authority.id.authority}
       </c:forEach>
  </p>
</c:forEach>
</body>
</html>
