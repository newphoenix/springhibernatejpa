package org.phoenix.app.dao;

import org.phoenix.app.model.Car;

public interface ICar {

	void persist(Car transientInstance);

	void remove(Car persistentInstance);

	Car merge(Car detachedInstance);

	Car findById(Long id);

}