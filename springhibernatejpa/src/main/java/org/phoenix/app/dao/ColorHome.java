package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Color;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Color.
 * @see org.phoenix.app.model.Color
 * @author Hibernate Tools
 */
@Service("colorService") 
@Repository
@Transactional
public class ColorHome implements IColor {

	private static final Log log = LogFactory.getLog(ColorHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IColor#persist(org.phoenix.app.model.Color)
	 */
	@Override
	public void persist(Color transientInstance) {
		log.debug("persisting Color instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IColor#remove(org.phoenix.app.model.Color)
	 */
	@Override
	public void remove(Color persistentInstance) {
		log.debug("removing Color instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IColor#merge(org.phoenix.app.model.Color)
	 */
	@Override
	public Color merge(Color detachedInstance) {
		log.debug("merging Color instance");
		try {
			Color result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IColor#findById(java.lang.Integer)
	 */
	@Override
	public Color findById(Integer id) {
		log.debug("getting Color instance with id: " + id);
		try {
			Color instance = entityManager.find(Color.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
