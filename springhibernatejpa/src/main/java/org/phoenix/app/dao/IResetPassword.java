package org.phoenix.app.dao;

import org.phoenix.app.model.ResetPassword;

public interface IResetPassword {

	void persist(ResetPassword transientInstance);

	void remove(ResetPassword persistentInstance);

	ResetPassword merge(ResetPassword detachedInstance);

	ResetPassword findById(Integer id);

}