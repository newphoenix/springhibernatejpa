package org.phoenix.app.dao;

import org.phoenix.app.model.City;

public interface ICity {

	void persist(City transientInstance);

	void remove(City persistentInstance);

	City merge(City detachedInstance);

	City findById(Integer id);

}