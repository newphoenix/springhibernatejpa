package org.phoenix.app.dao;

import org.phoenix.app.model.Model;

public interface IModel {

	void persist(Model transientInstance);

	void remove(Model persistentInstance);

	Model merge(Model detachedInstance);

	Model findById(Integer id);

}