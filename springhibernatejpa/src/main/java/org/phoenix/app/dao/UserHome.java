package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class User.
 * @see org.phoenix.app.model.User
 * @author Hibernate Tools
 */
@Service("userService") 
@Repository 
@Transactional
public class UserHome implements IUser {

	private static final Log log = LogFactory.getLog(UserHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IUser#persist(org.phoenix.app.model.User)
	 */
	@Override
	public void persist(User transientInstance) {
		log.debug("persisting User instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IUser#remove(org.phoenix.app.model.User)
	 */
	@Override
	public void remove(User persistentInstance) {
		log.debug("removing User instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IUser#merge(org.phoenix.app.model.User)
	 */
	@Override
	public User merge(User detachedInstance) {
		log.debug("merging User instance");
		try {
			User result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IUser#findById(java.lang.Integer)
	 */
	@Override
	public User findById(Integer id) {
		log.debug("getting User instance with id: " + id);
		try {
			User instance = entityManager.find(User.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Override
	@Transactional(readOnly=true)
	@Cacheable(value="UserFindAllCache")
	public List<User> findAll() {		
		long t1 = System.currentTimeMillis();
		List<User> lst =  entityManager.createQuery("SELECT u from User u", User.class).getResultList();		
	    System.out.println("Time took: "+ (System.currentTimeMillis()-t1));
		return lst;
	}
	
	@Override
	@Transactional(readOnly=true)	
	public List<User> findAllNoCache() {		
		long t1 = System.currentTimeMillis();
		List<User> lst =  entityManager.createQuery("SELECT u from User u", User.class).getResultList();		
	    System.out.println("Time took no cache: "+ (System.currentTimeMillis()-t1));
		return lst;
	}
}
