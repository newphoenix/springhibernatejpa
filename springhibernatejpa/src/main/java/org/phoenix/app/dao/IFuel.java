package org.phoenix.app.dao;

import org.phoenix.app.model.Fuel;

public interface IFuel {

	void persist(Fuel transientInstance);

	void remove(Fuel persistentInstance);

	Fuel merge(Fuel detachedInstance);

	Fuel findById(Integer id);

}