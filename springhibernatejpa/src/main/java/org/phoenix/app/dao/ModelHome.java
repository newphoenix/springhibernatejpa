package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Model;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Model.
 * @see org.phoenix.app.model.Model
 * @author Hibernate Tools
 */
@Service("modelService") 
@Repository 
@Transactional
public class ModelHome implements IModel {

	private static final Log log = LogFactory.getLog(ModelHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IModel#persist(org.phoenix.app.model.Model)
	 */
	@Override
	public void persist(Model transientInstance) {
		log.debug("persisting Model instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IModel#remove(org.phoenix.app.model.Model)
	 */
	@Override
	public void remove(Model persistentInstance) {
		log.debug("removing Model instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IModel#merge(org.phoenix.app.model.Model)
	 */
	@Override
	public Model merge(Model detachedInstance) {
		log.debug("merging Model instance");
		try {
			Model result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IModel#findById(java.lang.Integer)
	 */
	@Override
	public Model findById(Integer id) {
		log.debug("getting Model instance with id: " + id);
		try {
			Model instance = entityManager.find(Model.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
