package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Car;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Car.
 * @see org.phoenix.app.model.Car
 * @author Hibernate Tools
 */
@Service("carService")
@Repository
@Transactional
public class CarHome implements ICar {

	private static final Log log = LogFactory.getLog(CarHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICar#persist(org.phoenix.app.model.Car)
	 */
	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICity#persist(org.phoenix.app.model.Car)
	 */
	
	@Override
	public void persist(Car transientInstance) {
		log.debug("persisting Car instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICar#remove(org.phoenix.app.model.Car)
	 */
	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICity#remove(org.phoenix.app.model.Car)
	 */
	
	@Override
	public void remove(Car persistentInstance) {
		log.debug("removing Car instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICar#merge(org.phoenix.app.model.Car)
	 */
	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICity#merge(org.phoenix.app.model.Car)
	 */

	@Override
	public Car merge(Car detachedInstance) {
		log.debug("merging Car instance");
		try {
			Car result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICar#findById(java.lang.Long)
	 */
	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ICity#findById(java.lang.Long)
	 */
	
	@Override
	public Car findById(Long id) {
		log.debug("getting Car instance with id: " + id);
		try {
			Car instance = entityManager.find(Car.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
