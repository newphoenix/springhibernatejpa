package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Drivetrain;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Drivetrain.
 * @see org.phoenix.app.model.Drivetrain
 * @author Hibernate Tools
 */
@Service("driverService")
@Repository
@Transactional
public class DrivetrainHome implements IDrivtrain {

	private static final Log log = LogFactory.getLog(DrivetrainHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IDrivtrain#persist(org.phoenix.app.model.Drivetrain)
	 */
	@Override
	public void persist(Drivetrain transientInstance) {
		log.debug("persisting Drivetrain instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IDrivtrain#remove(org.phoenix.app.model.Drivetrain)
	 */
	@Override
	public void remove(Drivetrain persistentInstance) {
		log.debug("removing Drivetrain instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IDrivtrain#merge(org.phoenix.app.model.Drivetrain)
	 */
	@Override
	public Drivetrain merge(Drivetrain detachedInstance) {
		log.debug("merging Drivetrain instance");
		try {
			Drivetrain result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IDrivtrain#findById(java.lang.Integer)
	 */
	@Override
	public Drivetrain findById(Integer id) {
		log.debug("getting Drivetrain instance with id: " + id);
		try {
			Drivetrain instance = entityManager.find(Drivetrain.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
