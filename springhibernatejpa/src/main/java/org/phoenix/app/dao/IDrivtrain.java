package org.phoenix.app.dao;

import org.phoenix.app.model.Drivetrain;

public interface IDrivtrain {

	void persist(Drivetrain transientInstance);

	void remove(Drivetrain persistentInstance);

	Drivetrain merge(Drivetrain detachedInstance);

	Drivetrain findById(Integer id);

}