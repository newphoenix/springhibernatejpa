package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Region;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Region.
 * @see org.phoenix.app.model.Region
 * @author Hibernate Tools
 */
@Service("regionService") 
@Repository 
@Transactional
public class RegionHome implements IRegion {

	private static final Log log = LogFactory.getLog(RegionHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegion#persist(org.phoenix.app.model.Region)
	 */
	@Override
	public void persist(Region transientInstance) {
		log.debug("persisting Region instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegion#remove(org.phoenix.app.model.Region)
	 */
	@Override
	public void remove(Region persistentInstance) {
		log.debug("removing Region instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegion#merge(org.phoenix.app.model.Region)
	 */
	@Override
	public Region merge(Region detachedInstance) {
		log.debug("merging Region instance");
		try {
			Region result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegion#findById(java.lang.Integer)
	 */
	@Override
	public Region findById(Integer id) {
		log.debug("getting Region instance with id: " + id);
		try {
			Region instance = entityManager.find(Region.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
