package org.phoenix.app.dao;

import org.phoenix.app.model.Country;

public interface ICountry {

	void persist(Country transientInstance);

	void remove(Country persistentInstance);

	Country merge(Country detachedInstance);

	Country findById(Integer id);

}