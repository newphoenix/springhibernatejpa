package org.phoenix.app.dao;

import org.phoenix.app.model.Manufacturer;

public interface IManufacturer {

	void persist(Manufacturer transientInstance);

	void remove(Manufacturer persistentInstance);

	Manufacturer merge(Manufacturer detachedInstance);

	Manufacturer findById(Integer id);

}