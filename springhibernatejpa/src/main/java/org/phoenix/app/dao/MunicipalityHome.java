package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Municipality;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Municipality.
 * @see org.phoenix.app.model.Municipality
 * @author Hibernate Tools
 */
@Service("municipalityService")
@Repository 
@Transactional
public class MunicipalityHome implements IMunicipality {

	private static final Log log = LogFactory.getLog(MunicipalityHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IMunicipality#persist(org.phoenix.app.model.Municipality)
	 */
	@Override
	public void persist(Municipality transientInstance) {
		log.debug("persisting Municipality instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IMunicipality#remove(org.phoenix.app.model.Municipality)
	 */
	@Override
	public void remove(Municipality persistentInstance) {
		log.debug("removing Municipality instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IMunicipality#merge(org.phoenix.app.model.Municipality)
	 */
	@Override
	public Municipality merge(Municipality detachedInstance) {
		log.debug("merging Municipality instance");
		try {
			Municipality result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IMunicipality#findById(java.lang.Integer)
	 */
	@Override
	public Municipality findById(Integer id) {
		log.debug("getting Municipality instance with id: " + id);
		try {
			Municipality instance = entityManager.find(Municipality.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
