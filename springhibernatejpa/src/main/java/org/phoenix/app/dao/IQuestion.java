package org.phoenix.app.dao;

import org.phoenix.app.model.Question;

public interface IQuestion {

	void persist(Question transientInstance);

	void remove(Question persistentInstance);

	Question merge(Question detachedInstance);

	Question findById(Integer id);

}