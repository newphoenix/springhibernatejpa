package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Fuel;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Fuel.
 * @see org.phoenix.app.model.Fuel
 * @author Hibernate Tools
 */
@Service("fuelService") 
@Repository 
@Transactional
public class FuelHome implements IFuel {

	private static final Log log = LogFactory.getLog(FuelHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IFuel#persist(org.phoenix.app.model.Fuel)
	 */
	@Override
	public void persist(Fuel transientInstance) {
		log.debug("persisting Fuel instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IFuel#remove(org.phoenix.app.model.Fuel)
	 */
	@Override
	public void remove(Fuel persistentInstance) {
		log.debug("removing Fuel instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IFuel#merge(org.phoenix.app.model.Fuel)
	 */
	@Override
	public Fuel merge(Fuel detachedInstance) {
		log.debug("merging Fuel instance");
		try {
			Fuel result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IFuel#findById(java.lang.Integer)
	 */
	@Override
	public Fuel findById(Integer id) {
		log.debug("getting Fuel instance with id: " + id);
		try {
			Fuel instance = entityManager.find(Fuel.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
