package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.TransmissionLevel;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class TransmissionLevel.
 * @see org.phoenix.app.model.TransmissionLevel
 * @author Hibernate Tools
 */
@Service("transmissionLevelService")
@Repository 
@Transactional
public class TransmissionLevelHome implements ITransmissionLevel {

	private static final Log log = LogFactory.getLog(TransmissionLevelHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmissionLevel#persist(org.phoenix.app.model.TransmissionLevel)
	 */
	@Override
	public void persist(TransmissionLevel transientInstance) {
		log.debug("persisting TransmissionLevel instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmissionLevel#remove(org.phoenix.app.model.TransmissionLevel)
	 */
	@Override
	public void remove(TransmissionLevel persistentInstance) {
		log.debug("removing TransmissionLevel instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmissionLevel#merge(org.phoenix.app.model.TransmissionLevel)
	 */
	@Override
	public TransmissionLevel merge(TransmissionLevel detachedInstance) {
		log.debug("merging TransmissionLevel instance");
		try {
			TransmissionLevel result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmissionLevel#findById(java.lang.Integer)
	 */
	@Override
	public TransmissionLevel findById(Integer id) {
		log.debug("getting TransmissionLevel instance with id: " + id);
		try {
			TransmissionLevel instance = entityManager.find(TransmissionLevel.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
