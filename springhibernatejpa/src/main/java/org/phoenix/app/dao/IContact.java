package org.phoenix.app.dao;

import org.phoenix.app.model.Contact;

public interface IContact {

	void persist(Contact transientInstance);

	void remove(Contact persistentInstance);

	Contact merge(Contact detachedInstance);

	Contact findById(Integer id);

}