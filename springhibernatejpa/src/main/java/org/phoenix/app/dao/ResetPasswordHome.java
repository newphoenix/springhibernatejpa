package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.ResetPassword;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class ResetPassword.
 * @see org.phoenix.app.model.ResetPassword
 * @author Hibernate Tools
 */
@Service("resetPasswordService")
@Repository 
@Transactional
public class ResetPasswordHome implements IResetPassword {

	private static final Log log = LogFactory.getLog(ResetPasswordHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IResetPassword#persist(org.phoenix.app.model.ResetPassword)
	 */
	@Override
	public void persist(ResetPassword transientInstance) {
		log.debug("persisting ResetPassword instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IResetPassword#remove(org.phoenix.app.model.ResetPassword)
	 */
	@Override
	public void remove(ResetPassword persistentInstance) {
		log.debug("removing ResetPassword instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IResetPassword#merge(org.phoenix.app.model.ResetPassword)
	 */
	@Override
	public ResetPassword merge(ResetPassword detachedInstance) {
		log.debug("merging ResetPassword instance");
		try {
			ResetPassword result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IResetPassword#findById(java.lang.Integer)
	 */
	@Override
	public ResetPassword findById(Integer id) {
		log.debug("getting ResetPassword instance with id: " + id);
		try {
			ResetPassword instance = entityManager.find(ResetPassword.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
