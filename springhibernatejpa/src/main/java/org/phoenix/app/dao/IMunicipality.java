package org.phoenix.app.dao;

import org.phoenix.app.model.Municipality;

public interface IMunicipality {

	void persist(Municipality transientInstance);

	void remove(Municipality persistentInstance);

	Municipality merge(Municipality detachedInstance);

	Municipality findById(Integer id);

}