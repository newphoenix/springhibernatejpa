package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Contact;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Contact.
 * @see org.phoenix.app.model.Contact
 * @author Hibernate Tools
 */
@Service("contactService")
@Repository
@Transactional
public class ContactHome implements IContact {

	private static final Log log = LogFactory.getLog(ContactHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IContact#persist(org.phoenix.app.model.Contact)
	 */
	@Override
	public void persist(Contact transientInstance) {
		log.debug("persisting Contact instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IContact#remove(org.phoenix.app.model.Contact)
	 */
	@Override
	public void remove(Contact persistentInstance) {
		log.debug("removing Contact instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IContact#merge(org.phoenix.app.model.Contact)
	 */
	@Override
	public Contact merge(Contact detachedInstance) {
		log.debug("merging Contact instance");
		try {
			Contact result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IContact#findById(java.lang.Integer)
	 */
	@Override
	public Contact findById(Integer id) {
		log.debug("getting Contact instance with id: " + id);
		try {
			Contact instance = entityManager.find(Contact.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
