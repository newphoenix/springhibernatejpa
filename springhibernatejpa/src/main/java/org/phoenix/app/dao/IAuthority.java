package org.phoenix.app.dao;

import org.phoenix.app.model.Authority;
import org.phoenix.app.model.AuthorityId;

public interface IAuthority {

	void persist(Authority transientInstance);

	void remove(Authority persistentInstance);

	Authority merge(Authority detachedInstance);

	Authority findById(AuthorityId id);

}