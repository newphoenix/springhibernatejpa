package org.phoenix.app.dao;

import org.phoenix.app.model.Transmission;

public interface ITransmission {

	void persist(Transmission transientInstance);

	void remove(Transmission persistentInstance);

	Transmission merge(Transmission detachedInstance);

	Transmission findById(Integer id);

}