package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Manufacturer;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Manufacturer.
 * @see org.phoenix.app.model.Manufacturer
 * @author Hibernate Tools
 */
@Service("manufacturerService") 
@Repository
@Transactional
public class ManufacturerHome implements IManufacturer {

	private static final Log log = LogFactory.getLog(ManufacturerHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IManufacturer#persist(org.phoenix.app.model.Manufacturer)
	 */
	@Override
	public void persist(Manufacturer transientInstance) {
		log.debug("persisting Manufacturer instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IManufacturer#remove(org.phoenix.app.model.Manufacturer)
	 */
	@Override
	public void remove(Manufacturer persistentInstance) {
		log.debug("removing Manufacturer instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IManufacturer#merge(org.phoenix.app.model.Manufacturer)
	 */
	@Override
	public Manufacturer merge(Manufacturer detachedInstance) {
		log.debug("merging Manufacturer instance");
		try {
			Manufacturer result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IManufacturer#findById(java.lang.Integer)
	 */
	@Override
	public Manufacturer findById(Integer id) {
		log.debug("getting Manufacturer instance with id: " + id);
		try {
			Manufacturer instance = entityManager.find(Manufacturer.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
