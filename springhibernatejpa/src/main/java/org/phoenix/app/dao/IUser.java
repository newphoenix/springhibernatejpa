package org.phoenix.app.dao;

import java.util.List;

import org.phoenix.app.model.User;

public interface IUser {

	void persist(User transientInstance);

	void remove(User persistentInstance);

	User merge(User detachedInstance);

	User findById(Integer id);
	List<User> findAll();

	List<User> findAllNoCache();

}