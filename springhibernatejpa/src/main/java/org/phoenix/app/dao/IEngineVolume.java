package org.phoenix.app.dao;

import org.phoenix.app.model.EngineVolume;

public interface IEngineVolume {

	void persist(EngineVolume transientInstance);

	void remove(EngineVolume persistentInstance);

	EngineVolume merge(EngineVolume detachedInstance);

	EngineVolume findById(Integer id);

}