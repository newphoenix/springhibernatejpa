package org.phoenix.app.dao;

import org.phoenix.app.model.Registration;

public interface IRegistration {

	void persist(Registration transientInstance);

	void remove(Registration persistentInstance);

	Registration merge(Registration detachedInstance);

	Registration findById(Integer id);

}