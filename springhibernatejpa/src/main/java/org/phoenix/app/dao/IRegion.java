package org.phoenix.app.dao;

import org.phoenix.app.model.Region;

public interface IRegion {

	void persist(Region transientInstance);

	void remove(Region persistentInstance);

	Region merge(Region detachedInstance);

	Region findById(Integer id);

}