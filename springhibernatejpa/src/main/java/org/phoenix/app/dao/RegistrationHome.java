package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Registration;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Registration.
 * @see org.phoenix.app.model.Registration
 * @author Hibernate Tools
 */
@Service("registrationService")
@Repository 
@Transactional
public class RegistrationHome implements IRegistration {

	private static final Log log = LogFactory.getLog(RegistrationHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegistration#persist(org.phoenix.app.model.Registration)
	 */
	@Override
	public void persist(Registration transientInstance) {
		log.debug("persisting Registration instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegistration#remove(org.phoenix.app.model.Registration)
	 */
	@Override
	public void remove(Registration persistentInstance) {
		log.debug("removing Registration instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegistration#merge(org.phoenix.app.model.Registration)
	 */
	@Override
	public Registration merge(Registration detachedInstance) {
		log.debug("merging Registration instance");
		try {
			Registration result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IRegistration#findById(java.lang.Integer)
	 */
	@Override
	public Registration findById(Integer id) {
		log.debug("getting Registration instance with id: " + id);
		try {
			Registration instance = entityManager.find(Registration.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
