package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Gender;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Gender.
 * @see org.phoenix.app.model.Gender
 * @author Hibernate Tools
 */
@Service("genderService")
@Repository 
@Transactional
public class GenderHome implements IGender {

	private static final Log log = LogFactory.getLog(GenderHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IGender#persist(org.phoenix.app.model.Gender)
	 */
	@Override
	public void persist(Gender transientInstance) {
		log.debug("persisting Gender instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IGender#remove(org.phoenix.app.model.Gender)
	 */
	@Override
	public void remove(Gender persistentInstance) {
		log.debug("removing Gender instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IGender#merge(org.phoenix.app.model.Gender)
	 */
	@Override
	public Gender merge(Gender detachedInstance) {
		log.debug("merging Gender instance");
		try {
			Gender result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IGender#findById(java.lang.Integer)
	 */
	@Override
	public Gender findById(Integer id) {
		log.debug("getting Gender instance with id: " + id);
		try {
			Gender instance = entityManager.find(Gender.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
