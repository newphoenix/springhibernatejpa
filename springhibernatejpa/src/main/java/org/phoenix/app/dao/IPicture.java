package org.phoenix.app.dao;

import org.phoenix.app.model.Picture;

public interface IPicture {

	void persist(Picture transientInstance);

	void remove(Picture persistentInstance);

	Picture merge(Picture detachedInstance);

	Picture findById(Integer id);

}