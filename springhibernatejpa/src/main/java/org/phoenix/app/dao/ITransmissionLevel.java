package org.phoenix.app.dao;

import org.phoenix.app.model.TransmissionLevel;

public interface ITransmissionLevel {

	void persist(TransmissionLevel transientInstance);

	void remove(TransmissionLevel persistentInstance);

	TransmissionLevel merge(TransmissionLevel detachedInstance);

	TransmissionLevel findById(Integer id);

}