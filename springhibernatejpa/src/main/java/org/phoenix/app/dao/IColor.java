package org.phoenix.app.dao;

import org.phoenix.app.model.Color;

public interface IColor {

	void persist(Color transientInstance);

	void remove(Color persistentInstance);

	Color merge(Color detachedInstance);

	Color findById(Integer id);

}