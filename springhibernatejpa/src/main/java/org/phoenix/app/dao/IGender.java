package org.phoenix.app.dao;

import org.phoenix.app.model.Gender;

public interface IGender {

	void persist(Gender transientInstance);

	void remove(Gender persistentInstance);

	Gender merge(Gender detachedInstance);

	Gender findById(Integer id);

}