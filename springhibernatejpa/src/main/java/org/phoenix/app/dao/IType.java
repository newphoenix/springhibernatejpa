package org.phoenix.app.dao;

import org.phoenix.app.model.Type;

public interface IType {

	void persist(Type transientInstance);

	void remove(Type persistentInstance);

	Type merge(Type detachedInstance);

	Type findById(Integer id);

}