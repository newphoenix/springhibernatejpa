package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Authority;
import org.phoenix.app.model.AuthorityId;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Authority.
 * @see org.phoenix.app.model.Authority
 * @author Hibernate Tools
 */
@Service("authorityService") 
@Repository 
@Transactional
public class AuthorityHome implements IAuthority {

	private static final Log log = LogFactory.getLog(AuthorityHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IAuthority#persist(org.phoenix.app.model.Authority)
	 */
	@Override
	public void persist(Authority transientInstance) {
		log.debug("persisting Authority instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IAuthority#remove(org.phoenix.app.model.Authority)
	 */
	@Override
	public void remove(Authority persistentInstance) {
		log.debug("removing Authority instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IAuthority#merge(org.phoenix.app.model.Authority)
	 */
	@Override
	public Authority merge(Authority detachedInstance) {
		log.debug("merging Authority instance");
		try {
			Authority result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IAuthority#findById(org.phoenix.app.model.AuthorityId)
	 */
	@Override
	public Authority findById(AuthorityId id) {
		log.debug("getting Authority instance with id: " + id);
		try {
			Authority instance = entityManager.find(Authority.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
