package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Transmission;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Transmission.
 * @see org.phoenix.app.model.Transmission
 * @author Hibernate Tools
 */
@Service("transmissionService")
@Repository 
@Transactional
public class TransmissionHome implements ITransmission {

	private static final Log log = LogFactory.getLog(TransmissionHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmission#persist(org.phoenix.app.model.Transmission)
	 */
	@Override
	public void persist(Transmission transientInstance) {
		log.debug("persisting Transmission instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmission#remove(org.phoenix.app.model.Transmission)
	 */
	@Override
	public void remove(Transmission persistentInstance) {
		log.debug("removing Transmission instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmission#merge(org.phoenix.app.model.Transmission)
	 */
	@Override
	public Transmission merge(Transmission detachedInstance) {
		log.debug("merging Transmission instance");
		try {
			Transmission result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.ITransmission#findById(java.lang.Integer)
	 */
	@Override
	public Transmission findById(Integer id) {
		log.debug("getting Transmission instance with id: " + id);
		try {
			Transmission instance = entityManager.find(Transmission.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
