package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Type;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class Type.
 * @see org.phoenix.app.model.Type
 * @author Hibernate Tools
 */
@Service("typeService")
@Repository 
@Transactional
public class TypeHome implements IType {

	private static final Log log = LogFactory.getLog(TypeHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IType#persist(org.phoenix.app.model.Type)
	 */
	@Override
	public void persist(Type transientInstance) {
		log.debug("persisting Type instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IType#remove(org.phoenix.app.model.Type)
	 */
	@Override
	public void remove(Type persistentInstance) {
		log.debug("removing Type instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IType#merge(org.phoenix.app.model.Type)
	 */
	@Override
	public Type merge(Type detachedInstance) {
		log.debug("merging Type instance");
		try {
			Type result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IType#findById(java.lang.Integer)
	 */
	@Override
	public Type findById(Integer id) {
		log.debug("getting Type instance with id: " + id);
		try {
			Type instance = entityManager.find(Type.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
