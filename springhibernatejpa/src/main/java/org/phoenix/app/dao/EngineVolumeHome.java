package org.phoenix.app.dao;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.EngineVolume;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Home object for domain model class EngineVolume.
 * @see org.phoenix.app.model.EngineVolume
 * @author Hibernate Tools
 */
@Service("engineVolumeService")
@Repository
@Transactional
public class EngineVolumeHome implements IEngineVolume {

	private static final Log log = LogFactory.getLog(EngineVolumeHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IEnfineVolume#persist(org.phoenix.app.model.EngineVolume)
	 */
	@Override
	public void persist(EngineVolume transientInstance) {
		log.debug("persisting EngineVolume instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IEnfineVolume#remove(org.phoenix.app.model.EngineVolume)
	 */
	@Override
	public void remove(EngineVolume persistentInstance) {
		log.debug("removing EngineVolume instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IEnfineVolume#merge(org.phoenix.app.model.EngineVolume)
	 */
	@Override
	public EngineVolume merge(EngineVolume detachedInstance) {
		log.debug("merging EngineVolume instance");
		try {
			EngineVolume result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.dao.IEnfineVolume#findById(java.lang.Integer)
	 */
	@Override
	public EngineVolume findById(Integer id) {
		log.debug("getting EngineVolume instance with id: " + id);
		try {
			EngineVolume instance = entityManager.find(EngineVolume.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
