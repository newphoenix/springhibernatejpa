package org.phoenix.app.model;
// Generated Dec 22, 2015 12:39:22 PM by Hibernate Tools 4.3.1.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ResetPassword generated by hbm2java
 */
@Entity
@Table(name = "reset_password", catalog = "car_db")
public class ResetPassword implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4711183768484933955L;
	private Integer id;
	private String email;
	private String confirmationCode;
	private Date created;
	private String createdBy;
	private Date modified;
	private String modifiedBy;

	public ResetPassword() {
	}

	public ResetPassword(String email, String confirmationCode, Date created, String createdBy) {
		this.email = email;
		this.confirmationCode = confirmationCode;
		this.created = created;
		this.createdBy = createdBy;
	}

	public ResetPassword(String email, String confirmationCode, Date created, String createdBy, Date modified,
			String modifiedBy) {
		this.email = email;
		this.confirmationCode = confirmationCode;
		this.created = created;
		this.createdBy = createdBy;
		this.modified = modified;
		this.modifiedBy = modifiedBy;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "email", nullable = false, length = 60)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "confirmation_code", nullable = false, length = 60)
	public String getConfirmationCode() {
		return this.confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", nullable = false, length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "created_by", nullable = false, length = 50)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified", length = 19)
	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Column(name = "modified_by", length = 50)
	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
